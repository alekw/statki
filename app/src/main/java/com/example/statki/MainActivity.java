package com.example.statki;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityManagerCompat;

import  android.app.ActivityManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity  {

    static GridView gridView;
    static TextView status, score;

    static Field[] fields = new Field[100];
    static Field[] hisFields = new Field[100];
    static FieldAdapter fieldAdapter;
    static FieldAdapter fieldAdapter2;

    static int shipsSizes[] = new int[]{4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
    //static int shipsSizes[] = new int[]{4, 3};

    static boolean shipDoneTwice = false;

    static int Npos;
    static int Spos;
    static int Epos;
    static int Wpos;
    static int prevPos;

    static ArrayList<Ship> ships = new ArrayList<Ship>();
    static ArrayList<Ship> hisSunkenShips = new ArrayList<Ship>();
    static ArrayList<Integer> myShotsHits = new ArrayList<Integer>();
    static ArrayList<Integer> myShotsMissed = new ArrayList<Integer>();

    public static void redrawFields(Field[] _fields, ArrayList<Ship> _ships, Field fieldType, boolean checkMyShips)
    {
        for(int i = 0; i < _fields.length; i++)
        {
            _fields[i] = Field.Sea;
        }

        if(!checkMyShips){
            for(int i = 0; i < myShotsHits.size(); i++)
            {
                int pos = myShotsHits.get(i);
                _fields[pos] = Field.Hit;
            }
            for(int i = 0; i < myShotsMissed.size(); i++)
            {
                int pos = myShotsMissed.get(i);
                _fields[pos] = Field.Missed;
            }
        }

        for(int i = 0; i < _ships.size(); i++)
        {
            Ship t = _ships.get(i);

            for(int j = 0; j < t.fields.size(); j++)
            {
                int p = t.fields.get(j);
                _fields[p] = fieldType;
            }


        }


    }

    public static boolean checkShoot(int pos)
    {
        for(int i = 0; i < ships.size(); i++)
        {
            Ship t = ships.get(i);

            for(int j = 0; j < t.fields.size(); j++)
            {
                int p = t.fields.get(j);
                if(p == pos)
                {
                    //
                    //t.fields.remove(j)
                    ships.get(i).size--;
                    return true;
                }
            }
        }

        return false;
    }

    public static String checkSunkenShips()
    {
        String result = "SUNKEN;";

        for(int i = 0; i < ships.size(); i++)
        {
            Ship t = ships.get(i);

            if(t.size <= 0)
            {
                for(int j = 0 ; j < t.fields.size(); j++)
                {
                    result += t.fields.get(j).toString() + ",";
                }
                result += ";";
            }

        }
        return result;
    }

    static boolean settingShips = true;

    public static void setShips(int position)
    {
        if(Ship.shipCounter < shipsSizes.length)
        {
            //rozstawiamy statki

            int length = shipsSizes[Ship.shipCounter];



            if(!shipDoneTwice)
            {
                prevPos = position;
                fields[position] = Field.Sunk;

                if (position % 10 + length <= 10) {
                    //da się w prawo
                    Epos = position + length - 1;
                    fields[Epos] = Field.Missed;
                }
                if (position % 10 - length + 1 >= 0) {
                    //da się w lewo
                    Wpos = position - length + 1;
                    fields[Wpos] = Field.Missed;
                }
                if (((int) (position / 10)) + length <= 10) {
                    //da sie w dół
                    Spos = position + (length - 1) * 10;
                    fields[Spos] = Field.Missed;
                }
                if (((int) (position / 10)) - length + 1 >= 0) {
                    //da sie w gore
                    Npos = position - (length - 1) * 10;
                    fields[Npos] = Field.Missed;
                }
            }

            if(shipDoneTwice || length == 1)
            {
                Ship temp = new Ship();

                int step = 1;

                if(length == 1)
                {
                    step = 1;
                }
                else if(position == Npos)
                {
                    step = 10;

                }
                else if(position == Spos)
                {
                    step = 10;

                }
                else if(position == Wpos)
                {
                    step = 1;
     //               status.setText(position + " "+prevPos+ " "+ Epos+ " "+Wpos+ " "+ position);

                }
                else if(position == Epos)
                {
                    step = 1;

                }
                else {
                    //status.setText(Npos + " "+Spos+ " "+ Epos+ " "+Wpos+ " "+ position);
                    status.setText("Wskaż poprawny koniec statku o dł." + length);
                    return;
                }
       //         status.setText(position + " "+prevPos+ " "+ Epos+ " "+Wpos+ " "+ position);

                redrawFields(fields, ships, Field.Ship, true);
                if(temp.generateShipByRange(position, prevPos, step))
                {
                    //ship ok
                    temp.size = length;
                    ships.add(temp);

                    Ship.shipCounter++;
                    if(Ship.shipCounter >= shipsSizes.length)
                    {
                        settingShips = false;
                        //dodano wszystkie statki

                        if(!isReady)
                        {
                            //randomize first person
                            Random rd = new Random();
                            myTurn = rd.nextBoolean();
                        }
                        wfd.sendMessage("READY;" + !myTurn);

                        //adapter to show his fields
                        gridView.setAdapter(fieldAdapter2);
                        fieldAdapter2.notifyDataSetChanged();

                        if(isConnected)
                        {
                            if(isReady)
                            {
                                if(myTurn)
                                {
                                    status.setText("Twój ruch!");
                                }
                                else {
                                    status.setText("Ruch przeciwnika...");
                                }
                            }
                            else{
                                status.setText("Przeciwnik jeszcze rozstawia statki. Czekaj...");
                            }
                        }
                        else{
                            status.setText("Przeciwnik niepołączony. Czekaj...");
                        }
                    }
                    else {
                        status.setText("Rozstaw statek o długości " + shipsSizes[Ship.shipCounter]);
                    }

                }
                else {
                    status.setText("Statek nachodzi na inny statek! " + shipsSizes[Ship.shipCounter]);
                }

                redrawFields(fields, ships, Field.Ship, true);
            }
            else {
                status.setText("Wskaż koniec statku długości " + length);
            }

            if(length != 1)
            {
                shipDoneTwice = !shipDoneTwice;
            }


            fieldAdapter.notifyDataSetChanged();
            gridView.invalidateViews();


        }
    }

    public static void fieldClicked(Context context, int position)
    {
        if(settingShips)
        {
            setShips(position);
        }
        else {


            int myShotPosition = position;

            changeTurnUI(false);

            wfd.sendMessage("SHOT;" + myShotPosition);


            //status.setText(wfd.groupOwnerAddress + " " + wfd.amIOwner + " " + wfd.getMyIP());

            //redrawFields(fields, ships, Field.Ship, true);



        }

    }

    public static void changeTurnUI(boolean v)
    {
        myTurn = v;
        redrawFields(hisFields, hisSunkenShips, Field.Sunk, false);
        fieldAdapter2.notifyDataSetChanged();
        gridView.invalidateViews();

        if(myTurn)
        {
            status.setText("Twój ruch!");
        }
        else{
            status.setText("Czekam na ruch przeciwnika...");
        }

        score.setText("Przeciwnik: " + hisScore  + "\n" + "Ty: " + myShotsHits.size());

    }

    static boolean myTurn = false;
    static boolean isReady = false;
    static boolean isConnected = false;
    static int hisScore = 0;

    static public void odbierzStrzal(String s, Context ctx)
    {
        //IP;192.168.1.1
        //SHOT;99
        //HIT;99
        //MISS;99
        //SUNKEN;1,2,3,4;5,15;
        //READY;TRUE

        String[] data = s.split(";");
        if(data[0].equals("IP"))
        {
            //set another phone ip adfddress
            wfd.hisIP = data[1];

            //myTurn = true;
            //redrawFields(hisFields, hisSunkenShips, Field.Sunk, false);
            //fieldAdapter2.notifyDataSetChanged();
            //fieldAdapter.notifyDataSetChanged();
            //gridView.invalidateViews();
            //changeTurnUI(true);
            isConnected = true;
            status.setText("Przeciwnik połączony...");

            return;
        }
        if(data[0].equals("READY"))
        {
            isReady = true;
            if(!settingShips)
            {
                //I already decided, ignore his decision

            }
            else{
                myTurn = Boolean.parseBoolean(data[1]);
            }
            if(myTurn)
            {
                status.setText("Przeciwnik gotowy. Twój ruch!");
            }
            else{
                status.setText("Przeciwnik gotowy. Czekaj na jego ruch...");
            }

            gridView.invalidateViews();

            return;
        }



        if(data[0].equals("SHOT"))
        {

            int shotPosition = Integer.parseInt(data[1]);
            boolean isHit = checkShoot(shotPosition);

            if(isHit)
            {
                //on mnie trafił
                wfd.sendMessage("HIT;" + data[1]);
                hisScore++;

                changeTurnUI(false);
                if(hisScore >= maxScore)
                {
                    showDialog("Przegrałeś", "Exit", "Restart", ctx);
                }

            }
            else {
                //on mnie nie trafił
                wfd.sendMessage("MISS;" + data[1]);
                changeTurnUI(true);
            }

            String sunkenShips = checkSunkenShips();
            if(!sunkenShips.equals("SUNKEN;"))
            {
                //zatopil juz jakies statki, przeslij mu liste do UI
                wfd.sendMessage(sunkenShips);
            }

        }
        if(data[0].equals("HIT"))
        {
            //trafiłem go
            int shotPosition = Integer.parseInt(data[1]);
            //hisFields[shotPosition] = Field.Hit;
            myShotsHits.add(shotPosition);
            changeTurnUI(true);

            if(maxScore <= myShotsHits.size())
            {

                //wygrałem
                showDialog("Wygrałeś", "Exit", "Restart", ctx);

            }
        }
        if(data[0].equals("MISS"))
        {
            //nie trafiłem go
            int shotPosition = Integer.parseInt(data[1]);
            //hisFields[shotPosition] = Field.Missed
            myShotsMissed.add(shotPosition);
            changeTurnUI(false);
        }

        if(data[0].equals("SUNKEN"))
        {


            for(int i = 1; i< data.length; i++)
            {
                //for each sunken ship
                String shp = data[i];

                Ship temp = new Ship();

                String[] sunkenFields = shp.split(",");
                for(int j = 0; j < sunkenFields.length; j++)
                {
                    int sunkenPosition = Integer.parseInt(sunkenFields[j]);
                    temp.fields.add(sunkenPosition);
                    temp.size++;
                }

                hisSunkenShips.add(temp);


            }

            changeTurnUI(myTurn);

        }

        //it doesnt change here anything, w zasadzie to niepotrzebe
        //changeTurnUI(myTurn);
        //redrawFields(hisFields, hisSunkenShips, Field.Sunk, false);
        //fieldAdapter2.notifyDataSetChanged();
        //gridView.invalidateViews();
    }

    static void showDialog(String mainText, String posText, String negText, final Context ctx)
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
        builder1.setMessage(mainText);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                posText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ((Activity)ctx).finishAndRemoveTask();
                    }
                });

        
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    static public WFD wfd;
    static int maxScore = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wfd = new WFD(this);


        status = findViewById(R.id.textView1);
        status.setText("Rozstaw statek o długości 4.");
        score = findViewById(R.id.scoreText);
        score.setSingleLine(false);


        redrawFields(fields, ships, Field.Ship, true);
        redrawFields(hisFields, ships, Field.Ship, true);//JUST TO INITIALIZE!

        gridView = findViewById(R.id.gridView1);

        fieldAdapter = new FieldAdapter(this, fields);
        fieldAdapter2 = new FieldAdapter(this, hisFields);

        gridView.setAdapter(fieldAdapter);

        maxScore = 0;
        for(int i = 0; i < shipsSizes.length; i++)
        {
            maxScore += shipsSizes[i];
        }

    }

    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        wfd.receiver = new WDBroadcastReceiver(wfd.manager, wfd.channel, this);
        registerReceiver(wfd.receiver, wfd.intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(wfd.receiver);
    }

}
