package com.example.statki;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class FieldAdapter extends BaseAdapter {

    private Context context;
    private final Field[] fields;

    public FieldAdapter(Context context, Field[] fields) {
        this.context = context;
        this.fields = fields;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        Field currentField = fields[position];

        int cl = 0;
        switch (currentField)
        {
            case Hit:
                cl = Color.RED;
                break;
            case Sea:
                cl = Color.BLUE;
                break;
            case Ship:
                cl = Color.GRAY;
                break;
            case Sunk:
                cl = Color.BLACK;
                break;
            case Missed:
                cl = Color.YELLOW;
                break;
        }

        Button temp = new Button(context);
        if((MainActivity.myTurn && MainActivity.isReady && currentField == Field.Sea) || (MainActivity.settingShips && (currentField == Field.Sea || currentField == Field.Missed)))
        {
            //add listener only when my turn or setting ships


            temp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, String.valueOf(position), Toast.LENGTH_LONG).show();
                    MainActivity.fieldClicked(context, position);
                }
            });
        }
        else{
            if(currentField == Field.Sea)
            {
                temp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "Nie możesz wybrać tego pola drugi raz.", Toast.LENGTH_LONG).show();
                    }
                });
            }
            else{
                temp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "Poczekaj na ruch przeciwnika...", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }
        temp.setBackgroundColor(cl);


        return temp;

    }

    @Override
    public int getCount() {
        return fields.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}