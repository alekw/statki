package com.example.statki;

import android.util.Log;

import java.util.ArrayList;

import static java.util.Collections.swap;

public class Ship
{
        static int shipCounter = 0;
        int size = 0;
        ArrayList<Integer> fields;

        Ship(int size, ArrayList<Integer> fields)
        {
                this.size = size;
                this.fields = fields;
        }
        Ship()
        {
                this.fields = new ArrayList<Integer>();
        };

        public boolean generateShipByRange(int start, int end, int step)
        {

                if(start > end)
                {
                        int temp = start;
                        start = end;
                        end = temp;
                }

                for(int i = start; i <= end; i += Math.abs(step))
                {
                        //Log.v("FIELD "+i, String.valueOf(MainActivity.fields[i].getFieldCode()));
                        if(MainActivity.fields[i] == Field.Ship)
                        {
                                //area occuppied
                                return false;
                        }
                }

                this.fields.clear();


                for(int i = start; i <= end; i += Math.abs(step))
                {
                        this.fields.add(i);
                        this.size++;
                }

                return true;

        }
}