package com.example.statki;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.WIFI_P2P_SERVICE;
import static android.os.Looper.getMainLooper;

public class WFD {

    public final IntentFilter intentFilter = new IntentFilter();

    public WifiP2pManager manager;
    public WifiP2pManager.Channel channel;
    public WDBroadcastReceiver receiver;

    public List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    public static String groupOwnerAddress;
    public static boolean amIOwner = false;
    public static boolean amIConnected = false;
    public static String myIP, hisIP;
    public static int myPORT, hisPORT;

    public Context c;
    public WifiP2pManager.PeerListListener peerListListener;

    WFD(Context ctx)
    {
        c = ctx;
        // Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);


        manager = (WifiP2pManager) c.getSystemService(WIFI_P2P_SERVICE);
        channel = manager.initialize(c, getMainLooper(), null);

        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // Code for when the discovery initiation is successful goes here.
                // No services have actually been discovered yet, so this method
                // can often be left blank. Code for peer discovery goes in the
                // onReceive method, detailed below.
                //Log.d("WIFI DIRECT", "DISCOVER_PEERS_SUCCESS");
            }

            @Override
            public void onFailure(int reasonCode) {
                // Code for when the discovery initiation fails goes here.
                // Alert the user that something went wrong.
                //Log.d("WIFI DIRECT", "DISCOVER_PEERS_FAILURE");
            }
        });

        peerListListener = new WifiP2pManager.PeerListListener() {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList peerList) {

                ArrayList<WifiP2pDevice> refreshedPeers = new ArrayList<WifiP2pDevice>(peerList.getDeviceList());
                if (!refreshedPeers.equals(peers)) {
                    peers.clear();
                    peers.addAll(refreshedPeers);

                    // If an AdapterView is backed by this data, notify it
                    // of the change. For instance, if you have a ListView of
                    // available peers, trigger an update.
                    //((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();

                    // Perform any other updates needed based on the new list of
                    // peers connected to the Wi-Fi P2P network.
                }

                if (peers.size() == 0) {
                    Log.d("WIFI DIRECT", "No devices found");
                    return;
                }
                else {
                    Log.d("WIFI DIRECT", "Devices found");

                    for( WifiP2pDevice peer : peers)
                    {
                        Log.d("WIFI DIRECT", peer.deviceName);
                    }


                    if(!amIConnected) connect();
                }
            }
        };
    }

    public void connect() {
        for(int i = 0; i < peers.size(); i++)
        {
            // Picking the first device found on the network.
            WifiP2pDevice device = peers.get(i);

            WifiP2pConfig config = new WifiP2pConfig();
            config.deviceAddress = device.deviceAddress;
            config.wps.setup = WpsInfo.PBC;

            manager.connect(channel, config, new WifiP2pManager.ActionListener() {

                @Override
                public void onSuccess() {
                    // WiFiDirectBroadcastReceiver notifies us. Ignore for now.
                    //Log.d("WIFI FIRECT", "ON_SUCCESS");

                }

                @Override
                public void onFailure(int reason) {
                    //Log.d("WIFI FIRECT", "ON_FAILURE");
                    //Toast.makeText(MainActivity.this, "Connect failed. Retry.",Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    public static String getIpAddress() {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());

             for (NetworkInterface networkInterface : interfaces) { Log.v(TAG,
             "interface name " + networkInterface.getName() + "mac = " +
             getMACAddress(networkInterface.getName())); }


            for (NetworkInterface intf : interfaces) {
                //if (!getMACAddress(intf.getName()).equalsIgnoreCase(
                  //      Globals.thisDeviceAddress)) {
                    // Log.v(TAG, "ignore the interface " + intf.getName());
                    // continue;
                //}
                if (!intf.getName().contains("p2p"))
                    continue;

                Log.v(TAG,
                        intf.getName() + "   " + getMACAddress(intf.getName()));

                List<InetAddress> addrs = Collections.list(intf
                        .getInetAddresses());

                for (InetAddress addr : addrs) {
                    // Log.v(TAG, "inside");

                    if (!addr.isLoopbackAddress()) {
                        // Log.v(TAG, "isnt loopback");
                        String sAddr = addr.getHostAddress().toUpperCase();
                        Log.v(TAG, "ip=" + sAddr);

                        boolean isIPv4 = true;//InetAddressUtils.isIPv4Address(sAddr);

                        if (isIPv4) {
                            if (sAddr.contains("192.168.49.")) {
                                Log.v(TAG, "ip = " + sAddr);
                                return sAddr;
                            }
                        }

                    }

                }
            }

        } catch (Exception ex) {
            Log.v(TAG, "error in parsing");
        } // for now eat exceptions
        Log.v(TAG, "returning empty ip address");
        return "";
    };


    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName))
                        continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null)
                    return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0)
                    buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*
         * try { // this is so Linux hack return
         * loadFileAsString("/sys/class/net/" +interfaceName +
         * "/address").toUpperCase().trim(); } catch (IOException ex) { return
         * null; }
         */
    }

    public String getMyIP()
    {
        String ip = getIpAddress();
        Log.d("WIFI DIRECT PEER IP", ip);
        return ip;
    }

    boolean socketPrepared = false;

    public void prepareSocket() throws Exception
    {
        if(socketPrepared) return;

        myIP = getMyIP();

        if(!amIOwner)
        {

            hisIP = groupOwnerAddress;
            myPORT = 9998;
            hisPORT = 9999;

            Log.v("WIFI DIRECT", "JESTEM KLIENTEM");
            startServerSocket(myPORT);
            Log.v("WIFI DIRECT", "WYSYLAM SWOJE IP DO SERWERA");
            sendMessage("IP;" + myIP);        //send my IP to GO
            MainActivity.isConnected = true;



        }
        else {
            myPORT = 9999;
            hisPORT = 9998;
            Log.v("WIFI DIRECT", "JESTEM SERWEREM");
            startServerSocket(myPORT);
        }

        socketPrepared = true;

    }

    private void startServerSocket(final int port) {

        Thread thread = new Thread(new Runnable() {

            private String stringData = null;

            @Override
            public void run() {

                try {

                    ServerSocket ss = new ServerSocket(port);

                    while (true) {
                        //Log.v("WIFI DIRECT", "CZEKAM");
                        //Server is waiting for client here, if needed
                        Socket s = ss.accept();
                        BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        PrintWriter output = new PrintWriter(s.getOutputStream());

                        stringData = input.readLine();

                        ((Activity)c).runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                MainActivity.odbierzStrzal(stringData, c);
                            }
                        });


                        Log.v("WIFI DIRECT", "SERVER RECEIVED: " + stringData);
                        Log.v("WIFI DIRECT", "SERVER REPLY: " + "OK");
                        output.println("OK" + stringData.toUpperCase());
                        output.flush();

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //updateUI(stringData);
                        if (stringData.equalsIgnoreCase("STOP")) {
                         //   end = true;
                            output.close();
                            s.close();
                            break;
                        }

                        output.close();
                        s.close();
                    }
                    ss.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        thread.start();
    }

    public void sendMessage(final String msg) {
        Log.v("WIFI DIRECT", "SENDMESSAGE: " + msg);

//        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //Replace below IP with the IP of that device in which server socket open.
                    //If you change port then change the port number in the server side code also.
                    Socket s = new Socket( hisIP, hisPORT);

                    OutputStream out = s.getOutputStream();

                    PrintWriter output = new PrintWriter(out);

                    output.println(msg);
                    output.flush();
                    //Log.v("WIFI DIRECT", "SENDMESSAGE WYSLANO");
                    BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    final String st = input.readLine();

                    Log.v("WIFI DIRECT", "SENDMESSAGE REPLY: " + st);

    /*                handler.post(new Runnable() {
                        @Override
                        public void run() {


                            //String s = mTextViewReplyFromServer.getText().toString();
                            //if (st.trim().length() != 0)
//                                mTextViewReplyFromServer.setText(s + "\nFrom Server : " + st);
                        }
                    });*/

                    output.close();
                    out.close();
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }






}
