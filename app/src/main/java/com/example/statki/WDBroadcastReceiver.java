package com.example.statki;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.util.ArrayList;

public class WDBroadcastReceiver extends BroadcastReceiver
{
    WifiP2pManager manager;
    WifiP2pManager.Channel channel;
    Context activity;

    WDBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel, Context ctx)
    {
        this.manager = manager;
        this.channel = channel;
        this.activity = ctx;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Determine if Wifi P2P mode is enabled or not, alert
            // the Activity.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                Log.d("WIFI DIRECT", "WIFI_P2P_STATE_ENABLED");
                //activity.setIsWifiP2pEnabled(true);
            } else {
                Log.d("WIFI DIRECT", "WIFI_P2P_STATE_DISABLED");
                //activity.setIsWifiP2pEnabled(false);
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            // Request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if (manager != null) {
                manager.requestPeers(channel, MainActivity.wfd.peerListListener);
            }
            Log.d("WIFI DIRECT", "P2P peers changed");
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            Log.d("WIFI DIRECT", "WIFI_P2P_CONNECTION_CHANGED_ACTION");
            // Connection state changed! We should probably do something about
            // that.
            if (manager == null) {
                Log.d("WIFI DIRECT", "MANAGER_NULL");
                return;
            }

            final NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {
                Log.d("WIFI DIRECT", "IS_CONNECTED");
                // We are connected with the other device, request connection
                // info to find group owner IP

                manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(WifiP2pInfo info) {
                        if(info != null)
                        {
                            //Log.d("WIFI DIRECT", "CONN_INFO_NOT_NULL");
                            // InetAddress from WifiP2pInfo struct.
                            if(!WFD.amIConnected) {
                                String groupOwnerAddress = info.groupOwnerAddress.getHostAddress();
                                WFD.groupOwnerAddress = groupOwnerAddress;
                                WFD.amIOwner = info.isGroupOwner;
                                WFD.amIConnected = networkInfo.isConnected();

                                try {
                                    Log.v("WIFI DIRECT", "URUCHOM PREPARESOCKET");
                                    MainActivity.wfd.prepareSocket();
                                } catch (Exception e) {
                                }
                                ;
                            }

                            // After the group negotiation, we can determine the group owner
                            // (server).
                            if (info.groupFormed && info.isGroupOwner) {
                                //Log.d("WIFI DIRECT", "GROUP_OWNER");
                                // Do whatever tasks are specific to the group owner.
                                // One common case is creating a group owner thread and accepting
                                // incoming connections.
                            } else if (info.groupFormed) {
                                //Log.d("WIFI DIRECT", "GROUP_PEER");
                                // The other device acts as the peer (client). In this case,
                                // you'll want to create a peer thread that connects
                                // to the group owner.
                            }
                        }
                        else {
                            //Log.d("WIFI DIRECT", "CONN_INFO_NULL");
                        }
                    }
                });
            }
            else{Log.d("WIFI DIRECT", "NOT_CONNECTED");}
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            //DeviceListFragment fragment = (DeviceListFragment) activity.getFragmentManager().findFragmentById(R.id.frag_list);
            //fragment.updateThisDevice((WifiP2pDevice) intent.getParcelableExtra(
               //     WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));
            //Log.d("WIFI DIRECT", "WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");

        }
    }
}