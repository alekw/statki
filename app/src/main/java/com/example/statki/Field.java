package com.example.statki;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public enum Field
{
        Sea (0),
        Missed (1),
        Hit (2),
        Sunk (3),
        Ship (4);

        private final int fieldCode;

        Field(int code)
        {
            this.fieldCode = code;
        }

        public int getFieldCode()
        {
            return this.fieldCode;
        }
}